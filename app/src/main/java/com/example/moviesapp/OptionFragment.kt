package com.example.moviesapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.moviesapp.databinding.FragmentOptionBinding
import com.example.moviesapp.viewModel.ViewModel

class OptionFragment : Fragment() {

    private lateinit var bind : FragmentOptionBinding
    private val model : ViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bind = FragmentOptionBinding.inflate(layoutInflater)
        return bind.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bind.IAButton.setOnClickListener {

            model.getFromCAtegory(bind.ETCategory.text.toString())
            findNavController().navigate(R.id.action_option_to_llistaActor)
        }

        bind.IMbutton.setOnClickListener {
//            print("lista de mejores peliculas")
            findNavController().navigate(R.id.action_option_to_llistaMovies)
        }

        bind.fabButton.setOnClickListener {
            findNavController().navigate(R.id.action_optionFragment_to_favorateList)
        }
    }
}