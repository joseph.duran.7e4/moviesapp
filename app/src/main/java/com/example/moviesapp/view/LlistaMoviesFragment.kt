package com.example.moviesapp.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.adapter.MovieAdapter
import com.example.moviesapp.databinding.FragmentMoviesBinding
import com.example.moviesapp.model.Movie
import com.example.moviesapp.viewModel.ViewModel


class LlistaMoviesFragment : Fragment() {
    private lateinit var bind : FragmentMoviesBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val model : ViewModel by activityViewModels()
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        bind = FragmentMoviesBinding.inflate(layoutInflater)
        return bind.root
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movieAdapter = MovieAdapter(this)
        linearLayoutManager = LinearLayoutManager(context)

        model.movies.observe(this,{
            movieAdapter.setMovies(it)
        })

        with (bind.MoviesRecyclerView) {
            layoutManager = linearLayoutManager
            setHasFixedSize(false)
            adapter = movieAdapter
        }
    }

    fun onClick(movie: Movie) {
        model.select(movie)
        findNavController().navigate(R.id.action_movies_to_detailMovie)
    }
}