package com.example.moviesapp.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.CategoryEntity

import com.example.moviesapp.R
import com.example.moviesapp.adapter.ActorAdapter
import com.example.moviesapp.databinding.FragmentActorBinding
import com.example.moviesapp.model.CategoryX
import com.example.moviesapp.viewModel.ViewModel

class LlistaActorFragment : Fragment() {

    private lateinit var bind : FragmentActorBinding
    private lateinit var category : CategoryEntity
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val model : ViewModel by activityViewModels()
    private lateinit var categoryAdapter: ActorAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentActorBinding.inflate(layoutInflater)
        return bind.root
    }



    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryAdapter = ActorAdapter(this)
        linearLayoutManager = LinearLayoutManager(context)

        model.category.observe(this,{
            categoryAdapter.setCategory(it)
        })

        with(bind.actorRecyclerVIew){
            layoutManager = linearLayoutManager
            setHasFixedSize(false)
            adapter = categoryAdapter
        }

    }

    fun onClick(actor: CategoryX) {
        model.select(actor)
        findNavController().navigate(R.id.action_llistaActorFragment_to_detailActorFragment)
    }



}