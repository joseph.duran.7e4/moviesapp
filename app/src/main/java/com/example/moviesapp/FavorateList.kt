package com.example.moviesapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.moviesapp.viewModel.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.adapter.FavoriteAdapter
import com.example.moviesapp.databinding.FragmentFavorateListBinding


class FavorateList : Fragment() {
    private lateinit var bind : FragmentFavorateListBinding
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val model : ViewModel by activityViewModels()
    private lateinit var favAdapter : FavoriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentFavorateListBinding.inflate(layoutInflater)
        return bind.root
    }

    @SuppressLint("FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favAdapter = FavoriteAdapter(this)
        linearLayoutManager = LinearLayoutManager(context)

        model.favCategory.observe(this, {
          favAdapter.setCategory(it)
        })

        with(bind.fabList){
            layoutManager = linearLayoutManager
            setHasFixedSize(false)
            adapter = favAdapter
        }

    }

    fun onClick(categoryEntity: CategoryEntity) {
        model.selectDB(categoryEntity)
        findNavController().navigate(R.id.action_favorateList_to_detailActorFragment)
    }


}
