package com.example.moviesapp;

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviesapp.databinding.FragmentDetailMovieBinding
import com.example.moviesapp.model.CategoryX
import com.example.moviesapp.model.Movie

import com.example.moviesapp.viewModel.ViewModel

class DetailMovieFragment : Fragment() {

    private lateinit var bind : FragmentDetailMovieBinding
    private val model : ViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentDetailMovieBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.selectedMovie.observe(viewLifecycleOwner, Observer {
            val movie = it

            Glide.with(requireContext())
                .load((movie as Movie).url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(bind.IMovie)
            bind.titleMovie.text = movie.title
            bind.description.text = movie.description


            bind.cbFavorite.setOnClickListener {
                val favMovie = CategoryEntity(movie.id, movie.title, movie.url)
                ContactApplication.database.categoryDao().addCategory(favMovie)
            }
        } )



    }

}
