package com.example.moviesapp;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


@Dao
interface CategoryDao {

    @Query("SELECT * FROM CategoryEntity")
    fun getAllCategory(): MutableList<CategoryEntity>
    @Insert
    fun addCategory(category: CategoryEntity)
    @Update
    fun updateCategory(category: CategoryEntity)
    @Delete
    fun deleteCategory(category: CategoryEntity)

}