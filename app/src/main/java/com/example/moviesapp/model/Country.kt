package com.example.moviesapp.model

data class Country(
    val key: String,
    val value: String
)