package com.example.moviesapp.model

data class Star(
    val id: String,
    val name: String
)