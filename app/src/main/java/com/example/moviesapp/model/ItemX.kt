package com.example.moviesapp.model

data class ItemX(
    val id: String,
    val imDbRating: String,
    val image: String,
    val title: String,
    val year: String
)