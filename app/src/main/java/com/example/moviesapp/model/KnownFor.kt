package com.example.moviesapp.model

data class KnownFor(
    val fullTitle: String,
    val id: String,
    val image: String,
    val role: String,
    val title: String,
    val year: String
)