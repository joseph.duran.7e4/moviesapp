package com.example.moviesapp.model

data class Company(
    val id: String,
    val name: String
)