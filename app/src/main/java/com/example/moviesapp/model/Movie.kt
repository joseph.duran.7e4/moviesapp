package com.example.moviesapp.model

data class Movie (val id: String, var title: String, var url: String, var description: String )