package com.example.moviesapp.model

data class Genre(
    val key: String,
    val value: String
)