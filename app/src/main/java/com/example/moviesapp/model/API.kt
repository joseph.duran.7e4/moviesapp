package com.example.moviesapp.model

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface API {


    @GET("Keyword/k_39p1ijtf/{keyword}")
     suspend fun getCategoryName(@Path("keyword") keyword : String) : Response<Category>

     @GET("MostPopularMovies/k_39p1ijtf")
     suspend fun getMostPopularMovies(): Response<MostPopularMovies>

     @GET("Title/k_39p1ijtf/{id}")
     suspend fun getAll(@Path("id") id: String) : Response<Title>

    companion object{

        val BASE_URL = "https://imdb-api.com/API/"

        fun create():API{
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(API::class.java)
        }



    }
}