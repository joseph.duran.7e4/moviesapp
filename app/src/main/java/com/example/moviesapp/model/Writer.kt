package com.example.moviesapp.model

data class Writer(
    val id: String,
    val name: String
)