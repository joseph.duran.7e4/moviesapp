package com.example.moviesapp.model

data class Language(
    val key: String,
    val value: String
)