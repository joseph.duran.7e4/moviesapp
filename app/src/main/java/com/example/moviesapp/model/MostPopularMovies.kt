package com.example.moviesapp.model

data class MostPopularMovies(
    val errorMessage: String,
    val items: List<Item>
)