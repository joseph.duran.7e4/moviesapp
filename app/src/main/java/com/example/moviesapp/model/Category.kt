package com.example.moviesapp.model

data class Category(
    val errorMessage: String,
    val items: List<ItemX>,
    val keyword: String
)