package com.example.moviesapp.model

data class Director(
    val id: String,
    val name: String
)