package com.example.moviesapp;

import androidx.room.Database;
import androidx.room.RoomDatabase

@Database(entities = arrayOf(CategoryEntity:: class), version = 1)
abstract class CategoryDataBase : RoomDatabase() {
    abstract fun categoryDao() : CategoryDao
}