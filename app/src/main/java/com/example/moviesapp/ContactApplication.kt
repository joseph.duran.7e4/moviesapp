package com.example.moviesapp

import android.app.Application
import androidx.room.Room

class ContactApplication  : Application(){
    companion object {
        lateinit var database: CategoryDataBase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            CategoryDataBase::class.java,
            "CategoryDatabase").build()
    }

}