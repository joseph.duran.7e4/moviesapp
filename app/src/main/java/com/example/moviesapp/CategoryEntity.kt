package com.example.moviesapp


import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "CategoryEntity")
data class CategoryEntity(
    @PrimaryKey
    var id: String,
    var name: String,
    var image: String
)
