package com.example.moviesapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviesapp.databinding.FragmentDetailActorBinding
import com.example.moviesapp.model.Category
import com.example.moviesapp.model.CategoryX
import com.example.moviesapp.model.Movie
import com.example.moviesapp.viewModel.ViewModel


class DetailActorFragment : Fragment() {
    private lateinit var bind : FragmentDetailActorBinding
    private val model : ViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentDetailActorBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.selectedCategory.observe(viewLifecycleOwner, Observer {
            val category = it

            Glide.with(requireContext())
                .load((category as CategoryX).image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(bind.imageActor)
            bind.nameActor.text = category.name


            bind.cbFavorite.setOnClickListener {
                val favMovie = CategoryEntity(category.id, category.name, category.image)
                ContactApplication.database.categoryDao().addCategory(favMovie)
            }
            
        } )

    }
}