package com.example.moviesapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviesapp.view.LlistaMoviesFragment
import com.example.moviesapp.model.Movie
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ItemMovieBinding

class MovieAdapter(private val listener: LlistaMoviesFragment
) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
        private lateinit var context: Context
        private var movies : MutableList<Movie> = mutableListOf()

        inner class ViewHolder(view : View): RecyclerView.ViewHolder(view){
            val binding = ItemMovieBinding.bind(view)
            fun setListener (movie : Movie){
                binding.root.setOnClickListener {
                    listener.onClick(movie)
                }
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        with(holder){
            setListener(movie)
            binding.textTitle.text = movie.title
            Glide.with(context)
                .load(movie.url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imageMovie)
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMovies(movies : MutableList<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }
}