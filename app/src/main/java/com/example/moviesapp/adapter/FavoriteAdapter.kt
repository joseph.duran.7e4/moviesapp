package com.example.moviesapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviesapp.CategoryEntity
import com.example.moviesapp.FavorateList
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ItemActorBinding
import com.example.moviesapp.model.CategoryX

class FavoriteAdapter(private val listener: FavorateList
) : RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {

    private lateinit var context : Context
    private var actors : MutableList<CategoryEntity> = mutableListOf()


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = ItemActorBinding.bind(view)
        fun setListener(categoryEntity: CategoryEntity){
            binding.root.setOnClickListener {
                listener.onClick(categoryEntity)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteAdapter.ViewHolder {
        context = parent.context
        val view  = LayoutInflater.from(context).inflate(R.layout.item_actor, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = actors[position]
        print(category)
        with (holder) {
            setListener(category)
            binding.nameActor.text = category.name
            Glide.with(context)
                .load(category.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imageActor)

        }
    }

    override fun getItemCount(): Int {
        return actors.size
    }


    @SuppressLint("NotifyDataSetChanged")
    fun setCategory(category: MutableList<CategoryEntity>) {
        this.actors = category
        notifyDataSetChanged()
    }
}