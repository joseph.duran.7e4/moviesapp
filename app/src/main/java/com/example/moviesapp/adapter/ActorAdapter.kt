package com.example.moviesapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviesapp.view.LlistaActorFragment
import com.example.moviesapp.databinding.ItemActorBinding
import com.example.moviesapp.model.CategoryX
import com.example.moviesapp.R as R

class ActorAdapter(
    private val listener: LlistaActorFragment
    ) : RecyclerView.Adapter<ActorAdapter.ViewHolder>() {

    private lateinit var context : Context
    private var actors : MutableList<CategoryX> = mutableListOf()


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = ItemActorBinding.bind(view)
        fun setListener(actor: CategoryX){
            binding.root.setOnClickListener {
                listener.onClick(actor)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActorAdapter.ViewHolder {
        context = parent.context
        val view  = LayoutInflater.from(context).inflate(R.layout.item_actor, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val actor = actors[position]
        with (holder) {
            setListener(actor)
            binding.nameActor.text = actor.name
                Glide.with(context)
                .load(actor.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .circleCrop()
                .into(binding.imageActor)

        }
    }

    override fun getItemCount(): Int {
        return actors.size
    }


    @SuppressLint("NotifyDataSetChanged")
    fun setCategory(category : MutableList<CategoryX>) {
        this.actors = category
        notifyDataSetChanged()
    }

}