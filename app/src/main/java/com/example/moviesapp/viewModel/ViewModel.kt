package com.example.moviesapp.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.CategoryEntity
import com.example.moviesapp.model.*
import kotlinx.coroutines.launch

class ViewModel : ViewModel() {

    val api = API.create()

    var category = MutableLiveData<MutableList<CategoryX>>()
    var selectedCategory = MutableLiveData<CategoryX>()

    var movies = MutableLiveData<MutableList<Movie>>()
    var selectedMovie = MutableLiveData<Movie>()

    var favCategory = MutableLiveData<MutableList<CategoryEntity>>()
    var selectCategoryEntity = MutableLiveData<CategoryEntity>()



    init {
//        val mostPopular = api.getMostPopularMOvies()
        viewModelScope.launch {
            val response = api.getMostPopularMovies()
            val mostPopularMovies= response.body()
            val moviesList = mutableListOf<Movie>()
            for (item in mostPopularMovies!!.items){
                val movie = Movie("",item.fullTitle,item.image,"")
                moviesList.add(movie)
            }
            movies.value = moviesList
            println(movies.value)
        }

    }

    fun getFromCAtegory(categoria: String) {
        viewModelScope.launch {
            val response = api.getCategoryName(categoria)
            val dramasCAtegory = response.body()
            val categoryList = mutableListOf<CategoryX>()
            for (categoryes in dramasCAtegory!!.items){
                println(categoryes.title)
                val category = CategoryX(categoryes.id, categoryes.image, categoryes.title)
                categoryList.add(category)
            }
            category.value = categoryList
            print(category.value)
        }
    }
    
    fun selectDB(categoryEntity: CategoryEntity) {
        viewModelScope.launch {
            selectCategoryEntity.postValue(categoryEntity)
        }
    }

    fun select(category: CategoryX) {
        viewModelScope.launch {
            selectedCategory.postValue(category)
        }

    }

    fun select(movie: Movie) {

        viewModelScope.launch {
//            val response = api.getAll(movie.id)
//            val all = response.body()
//            movie.description = all!!.plot
            selectedMovie.postValue(movie)
//            println(all)
        }
    }

}